# isard-hypervisor

Se trata de la imágen docker que creamos en IsardVDI para poder distribuïr el hipervisor *KVM/qemu* independientemente de la distribución en dónde se instale y también para mantener las versiones de librerias.

Miremos como hemos construïdo este pecular contenedor, que requiere de opciones avanzadas de *Docker*. Para ello analizaremos en:

- el repositorio: [https://gitlab.com/isard/isardvdi/-/tree/main/docker/hypervisor](https://gitlab.com/isard/isardvdi/-/tree/main/docker/hypervisor)

    Usamos el *build* por fases. De esta manera podemos incluso usar una distribución para compilar un programa y luego copiarlo y usarlo en otro contenedor con otra distribución.

-  el yml de compose: [https://gitlab.com/isard/isardvdi/-/blob/main/docker-compose-parts/hypervisor.yml](https://gitlab.com/isard/isardvdi/-/blob/main/docker-compose-parts/hypervisor.yml)

    Vemos que hay un volumen que mapea dispositivos (/dev/vfio:/dev/vfio). Los dispositivos podemos en Linux montarlos dentro del contenedor para que sean directamente accesibles.

    También usamos la opción *privileged* que nos permite saltarnos el aislamiento de docker al completo (en todos sus *namespaces*). También podríamos afinar mejor con las 'cap_add': [https://docs.docker.com/compose/compose-file/compose-file-v3/#cap_add-cap_drop](https://docs.docker.com/compose/compose-file/compose-file-v3/#cap_add-cap_drop)

