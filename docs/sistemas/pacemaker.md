# IsardVDI Master/Slave cluster Ubuntu 22.04

El clúster OpenSource que usamos el el Pacemaker/Corosync de [https://clusterlabs.org/](https://clusterlabs.org/)

# Instalar y activar paquetes necesarios

- pacemaker: Gestor de recursos
- pcs: utilidad de línia de comandos
- resource-agents: agentes para gestionar recursos

```
apt -y install pacemaker pcs resource-agents 
systemctl enable pacemaker
systemctl enable corosync
systemctl enable pcsd
```

# Configuración

En los tres nodos que conformarán el clúster:

1. Establecer password para el usuario del clúster `hacluster`:

    passwd hacluster

2. Destruir el cluster de este nodo (suele crear uno por defecto al instalar los paquetes):

    pcs cluster destroy

Desde uno de los nodos autorizamos a los tres y creamos nuestro clúster `ha_formacion`:

    pcs host auth nodo1 addr=10.10.20.11 nodo2 addr=10.10.20.12 nodo3 addr=10.10.20.13 -u hacluster

    pcs cluster setup ha_formacion nodo1 addr=10.10.20.11 nodo2 addr=10.10.20.12 nodo3 addr=10.10.20.13 --force

Ahora ya podemos iniciar y activar el clúster:

    pcs cluster start --all
 
    pcs cluster enable --all

    systemctl enable --now pcsd

Deberíamos ver un clúster con los tres nodos ya monitorizados:

    pcs status

# Stonith (fencing device)

Como ya comentamos lo más importante en un clúster es que se puedan `matar` los nodos entre ellos cuando hay un problema. Hay diferentes sistemas para poder hacer esto. La más habitual és una regleta de corriente `PDU`, pero hay otras:

- PDU: [https://www.cyberpower.com/eu/es/knowledge/buying-guide/pdu](https://www.cyberpower.com/eu/es/knowledge/buying-guide/pdu)
- IPMI: Es un miniordenador con red independiente del servidor que va integrado y que nos permite realizar todo tipo de operaciones, incluso con el servidor apagado. [https://www.thomas-krenn.com/en/wiki/IPMI_Basics](https://www.thomas-krenn.com/en/wiki/IPMI_Basics)
- SSH: Mediante la conexión ssh entre servidores pueden ejecutar órdenes de reinicio, pero no de parado ya que no podremos reiniciarlo sin otro sistema, como por ejemplo Wake-on-Lan.

Los recursos de stonith que pacemaker tiene disponibles los podemos encontrar en [https://github.com/ClusterLabs/fence-agents/tree/main/agents](https://github.com/ClusterLabs/fence-agents/tree/main/agents)

Nosotros vamos a usar un stonith de tipo ssh para el laboratorio virtual a partir de este repositorio: [https://github.com/nannafudge/fence_ssh](https://github.com/nannafudge/fence_ssh)

Descargamos el fichero y le damos permisos de ejecución:

    curl -sSL https://raw.githubusercontent.com/nannafudge/fence_ssh/master/fence_ssh > /usr/sbin/fence_ssh
    chmod 777 /usr/sbin/fence_ssh 

Y creamos un stonith por cada nodo:

    pcs stonith create stonith-nodo1 fence_ssh user=root hostname=nodo1 password=pirineus sudo=true pcmk_host_list="nodo1"

    pcs stonith create stonith-nodo2 fence_ssh user=root hostname=nodo2 password=pirineus sudo=true pcmk_host_list="nodo2"

    pcs stonith create stonith-nodo3 fence_ssh user=root hostname=nodo3 password=pirineus sudo=true pcmk_host_list="nodo3"

Podemos ver el estado de los stonith y el nodo en el que se están ejecutando con `pcs status`.

Y evitamos que se ejecuten en su nodo (un nodo no debería matarse a si mismo, alguien lo hará...):

    pcs constraint location add snodo1-avoid-nodo1 stonith-nodo1 nodo1 -INFINITY
    pcs constraint location add snodo2-avoid-nodo2 stonith-nodo2 nodo2 -INFINITY
    pcs constraint location add snodo3-avoid-nodo3 stonith-nodo3 nodo3 -INFINITY


Hay diferentes tipos de constraints. Aquí hemos usado la primera para definir que deben evitar ciertos nodos. [Documentación de constraint location en RedHat](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_high_availability_clusters/assembly_determining-which-node-a-resource-runs-on-configuring-and-managing-high-availability-clusters#proc_configuring-location-constraints-determining-which-node-a-resource-runs-on)

# Recursos

Tenemos múltiples recursos para los servicios y acciones de sistemas más habituales en [https://github.com/ClusterLabs/resource-agents/tree/main/heartbeat](https://github.com/ClusterLabs/resource-agents/tree/main/heartbeat).

El primer recurso que deberemos crear es el del almacenaje replicado DRBD:

## Resource DRBD

Antes de crear el recurso deberemos modificar la configuración que hicimos de drbd para que pueda ser gestionado por el clúster. Básicamente vamos a añadir dos apartados (`handlers` y `net`) que van a permitirnos definir como se realizará el fence y qué hacer al volver un nodo previamente desconectado.

NOTA: Antes de hacer esto asegurar a tener el drbd parado (`umount /mnt;    drbdadm down drbd1`).

```
resource drbd1 {

    handlers
    {
      fence-peer "/usr/lib/drbd/crm-fence-peer.9.sh";
      unfence-peer "/usr/lib/drbd/crm-unfence-peer.9.sh";
    }

    net
    {
      allow-two-primaries no;
      #fencing dont-care;
      fencing resource-and-stonith;
      after-sb-0pri discard-zero-changes;
      after-sb-1pri discard-secondary;
      after-sb-2pri disconnect;
    }

  volume 0 {
    meta-disk internal;
    ...
```

NOTA: Recordad a copiar el mismo fichero a los tres nodos.

Le estamos diciendo el script a ejecutar cuando tenga que matar o reiniciar un nodo por un problema de drbd y también que no permitimos dos primarios y que, en recuperación, descarte los datos del menos reciente (el que estaba en secundario).

La órden `fencing resource-and-stonith` es realmente la que le dice que hay un clúster pacemaker al que llamar para matar el nodo si hay un problema. Si quisiéramos volver a activar el drbd sin el Pacemaker deberíamos comentar esta línea y descomentar la superior `fencing dont-care`.

El recurso DRBD es un poco especial y necesita primero crear el recurso y luego definir cuantos pueden estar en `primary` y cuantos en `secondary`. Para el recurso los primarios son los `promoted` y el número máximo, que depende de los nodos drbd, es el `clone`.

    pcs resource create drbd ocf:linbit:drbd drbd_resource=drbd1 op monitor interval=10s
    pcs resource promotable drbd \
            promoted-max=1 promoted-node-max=1 clone-max=3 clone-node-max=1 \
            notify=true

Una vez realizado esto deberíamos ver que en breve el DRBD se pone en `Master` en un nodo y en `Slave` en los otros (primary y secondaries). Con el `drbdsetup status` podremos ver como los ha arrancado.

    pcs status

# Punto de montaje

Como en el drbd ya teníamos creado un sistema de ficheros `xfs` ahora crearemos un recurso de tipo `filesystem` que se monte en `/var/www/html` en cuanto arranque el drbd en máster:

    mkdir /var/www/html

Y el recurso:

    pcs resource create MountFs Filesystem \
    device=/dev/drbd1 directory=/var/www/html \
    fstype=xfs "options=defaults,noatime,nodiratime,noquota" op monitor interval=10s

Y el orden y la colocación del recurso `MountFs` con el máster DRBD:

    pcs constraint order promote drbd-clone then start MountFs symmetrical=true kind=Mandatory

    pcs constraint colocation add MountFS with master drbd-clone

Aquí estamos usando dos nuevas constraints:

- **order**: nos permitirá definir en qué orden hay que arrancar recursos. Está claro que primero deberemos promocional a máster el ddrbd en un nodo (promote) y luego arrancar el punto de montaje. [Documentación constraint order en RedHat](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_high_availability_clusters/assembly_determining-resource-order.adoc-configuring-and-managing-high-availability-clusters)
- **colocation**: el montaje no se puede realizar en cualquier nodo, solo en el que tenga el drbd en máster. [Documentación constraint colocation en RedHat](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_high_availability_clusters/assembly_colocating-cluster-resources.adoc_configuring-and-managing-high-availability-clusters)

Verificamos que el estado está bien:

    pcs status

## Recurso servidor web

Encima del punto de montaje que hemos creado podemos ahora crear por ejemplo una instalación de un servidor web con una página simple. Para ello instalaremos un servidor `nginx` en los tres nodos (y lo dejamos desactivado, el clúster será quién lo active/desactive):

    apt install nginx -y

    systemctl disable --now nginx

Y creamos un fichero web simple en el directorio desde dónde va a servir el nginx (/var/www/html/index.html). Hay que comprobar que el clúster tiene el punto de montaje activo y hacerlo en el nodo en dónde está activo ahora mismo (lsblk nos puede servir para comprobarlo, junto con el pcs status):

```
<html>
<body>
<h1>Laboratorio IsardVDI pacemaker</h1>
<p>Este servidor nginx está en HA</p>
</body>
</html>
```

Como el recurso se va a poder mover de un nodo a otro primero necesitaremos una dirección IP virtual. Esta IP será puesta o quitada de la interfaz del nodo según configuremos:

    pcs resource create VirtualIP ocf:heartbeat:IPaddr2 ip=10.10.20.10 cidr_netmask=32 op monitor interval=30s

    pcs constraint order MountFs then VirtualIP

    pcs constraint colocation add VirtualIP with MountFs

Creamos un recurso para el servicio del `nginx`, que será similar al recurso que hemos creado del punto de montaje:

    pcs resource create WebServer systemd:nginx

Y el orden y la colocación del recurso:

    pcs constraint order VirtualIP then WebServer

    pcs constraint colocation add WebServer with VirtualIP
    
Si todo ha ido bien ahora deberíamos poder ver en el clúster todos estos recursos arrancados:

    pcs status

Desde otro navegador en el clúster deberíamos de poder ver la web (http://10.10.20.10).


# Prueba de alta disponibilidad

Podemos parar una de las máquinas y volverla a arrancar desde la interfaz web de IsardVDI para simular un problema y ver si recupera.

También podemos ejecutar órdenes desde línia de comandos que deberían hacer notar al Pacemaker que algo no va bien y ejecutar la parada:

- systemctl stop nginx
- systemctl stop sshd

O directamente podemos ejecutar el `fence` desde línia de comandos:

    pcs stonith fence nodo1






