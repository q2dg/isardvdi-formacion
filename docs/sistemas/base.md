# Configuración base

Para montar un clúster de servidores deberemos tener en cuenta ciertos apartados

Partiremos de un escenario con tres escritorios virtuales Ubuntu, cada uno de ellos con tres redes:

- *default*: Coge IP por DHCP. Salida a Internet.
- *personal1*: No tiene DHCP. Configuraremos para conexión DRBD (réplica de dispositivo de bloques)
- *personal2*: No tiene DHCP. Configuraremos para conexión NFS (Servidor de ficheros)

# Hostname

Editamos /etc/hostname y ponemos en cada nodo: nodo1 / nodo2 / nodo3

El nombre del host (*uname -a*) que cogerá de la configuración en /etc/hostname es básica para los servicios de clúster como Pacemaker

# Hosts

Es importante también para los servicios que se puedan mapear los nombres de host a su ip.

Editamos el /etc/hosts

    10.10.10.11 nodo1-drbd 
    10.10.10.12 nodo2-drbd 
    10.10.10.13 nodo3-drbd 
    10.10.20.11 nodo1 nodo1-nfs
    10.10.20.12 nodo2 nodo2-nfs
    10.10.20.13 nodo3 nodo3-nfs

# Red

En los nuevos sistemas operativos Ubuntu la configuración de red por defecto se hace con el *netplan*. Usa el formato yaml que ya vimos con el docker compose para definir la configuración de red.

Configuramos las IPs de red:

- DHCP: Automática
- DRBD: 10.10.10.11 (nodo1), 10.10.10.12 (nodo2), 10.10.10.13 (nodo3)
-  NFS: 10.10.20.11 (nodo1), 10.10.20.12 (nodo2), 10.10.20.13 (nodo3)

Editamos el fichero */etc/netplan/00-installer-config.yaml* y definimos allí las redes (ejemplo para el nodo1):

    network:
        ethernets:
            enp1s0:
                dhcp4: true
            enp2s0:
                dhcp4: false
                addresses: [10.10.10.11/24]
            enp5s0:
                dhcp4: false
                addresses: [10.10.20.11/24]
    version: 2

Y la aplicamos con `netplan apply`. Más información sobre netplan en [https://netplan.io/examples](https://netplan.io/examples)

# Reloj de sistema

Es primordial que los diferentes servidores usen un servicio *NTP* (que en los nuevos sistemas ha pasado a ser *timedatectl*/*chrony*) para mantener sus relojes sincronizados.

    systemctl status systemd-timesyncd
    systemctl enable --now systemd-timesyncd

# Seguridad de sistema

Los sistemas RedHat/CentOS/Fedora tienen una seguridad reforzada con *selinux*, el mismo sistema que Android por ejemplo.

En nuestro caso, Debian/Ubuntu y variantes, tienen el equivalente *apparmor* [https://apparmor.net/](https://apparmor.net/), que desactivaremos durante las pruebas de laboratorio para evitar conflictos de permisos y servicios.

    aa-status
    aa-teardown
    aa-status
    systemctl disable apparmor 

# Seguridad de red

En producción deberemos tener un firewall configurado. Siempre se configurará al finalizar el laboratorio para evitar problemáticas durante las pruebas.

En nuestro caso el Ubuntu suele venir con el firewall *ufw* [https://servidordebian.org/es/wheezy/security/firewall/ufw](https://servidordebian.org/es/wheezy/security/firewall/ufw). En cualquier caso, si estuviera instalado lo desactivaremos durante las pruebas con el laboratorio para evitar problemáticas.

    systemctl disable --now ufw

