# DRBD: Distributed Replicated Block Device

El sistema de DRBD es una réplica de dispositivo de bloques (disco en crudo) por red. Podemos imaginarlo como un sistema RAID pero en red.

El rendimiento que proporciona es muy superior ante otros sistemas, especialmente si el I/O de dico es muy intensivo [https://linbit.com/blog/iops-world-record-broken-linbit-tops-14-8-million-iops/](https://linbit.com/blog/iops-world-record-broken-linbit-tops-14-8-million-iops/), aunque limita su escalabilidad [https://www.youtube.com/watch?v=tWxpwpXE_ps](https://www.youtube.com/watch?v=tWxpwpXE_ps).

Una instalación básica consta de un servidor con el DRBD en **primary** y otro servidor en **secondary**. Solo podremos acceder a los datos en el servidor en dónde esté en primario.

Podemos cambiar la configuración para poder tener el drbd en **primary en los servidores**. Esto tiene que ir acompañado siempre de un sistema de archivos que permita ser usado en clúster (hacer locks de ficheros entre los servidores) como GFS2 u OCF2. No nos servirán los habituales EXT4, XFS, NTFS...

La versión 9 permite además hasta 16 servidores, pero aún solo dos en primario de los 16.

## Instalación

Usaremos tres escritorios virtuales basados en la plantilla de Ubuntu Server que ya tiene tres interfaces de red configuradas.

- nodo1
- nodo2
- nodo3

Instalaremos el gestor en caliente de módulos en Kernel DKMS:

    apt install -y dkms

Necesitaremos ciertos paquetes. Los que encontramos en Ubuntu 22.04 actualmente són de la versión 8, pero en el repositorio de LINBIT podemos encontrar los paquetes para la versión 9.

    apt install curl -y

    wget https://packages.linbit.com/proxmox/dists/proxmox-7/drbd-9/pool/drbd-utils_9.23.1-1_amd64.deb
    dpkg -i drbd-utils_9.23.1-1_amd64.deb

    wget https://packages.linbit.com/proxmox/dists/proxmox-7/drbd-9/pool/drbd-dkms_9.2.2-1_all.deb
    dpkg -i drbd-dkms_9.2.2-1_all.deb 
    dkms status

## Configuración

Encontraremos una configuración de ejemplo en el directorio `/usr/share/doc/packages/drbd` y documentación en [https://linbit.com/drbd-user-guide/drbd-guide-9_0-en/](https://linbit.com/drbd-user-guide/drbd-guide-9_0-en/)

La configuración en uso la tendremos en `/etc/drbd.d/<resource>.res`, en dónde el recurso será el nombre que daremos al DRBD.

Veamos una configuración típica para dos nodos con DRBD para nuestro recurso */etc/drbd.d/drbd1.res*.

NOTA: /dev/vda2 es una partición sin uso del sistema que vamos a replicar a nivel de bloques entre los nodos.

```
resource drbd1 {
  volume 0 {
    meta-disk internal;
    device minor 1;
    disk "/dev/vda2";
 }

 on nodo1 {                    #<-- hostname must match `uname -n` output
  node-id   0;                 #<-- Assign a unique node-id
  address  10.10.10.11:7789;   #<-- IP Address to be used to connect to the node with port
 }

 on nodo2 {
  node-id   1;
  address  10.10.10.12:7789;
 }

 on nodo3 {
  node-id   2;
  address  10.10.10.13:7789;
 }

 connection-mesh {
  hosts nodo1 nodo2 nodo3;
 }
}
```

En esta configuración (que hay que copiar a los tres nodos) tenemos apartados:

- **volume**: Podemos tener replicados diferentes volúmenes. En este caso solo uno en un mismo dispositivo de bloque en los tres nodos. Tenemos que crear los metadatos del sistema DRBD en el disco.
- **nodos**: Definimos cada nodo, sus ips y sus puertos (nos servirá para configurar en producción correctamente el firewall)
- **connection-mesh**: Permite crear diferentes estructuras de réplica de datos. En nuestro caso será una malla entre nodos.

## Puesta en marcha

1. Creamos los metadatos en los tres nodos:

    drbdadm create-md drbd1

2. Iniciamos el drbd en los tres nodos y miramos su estado:

    drbdadm up drbd1

    drbdsetup status

3. Ponemos en el nodo1 el drbd en primario y podemos usarlo ya para poner datos

    drbdadm primary drbd1 --force

    mkfs.xfs /dev/drbd0

    mount /dev/drbd0 /mnt

    touch /mnt/fichero.txt

Si todo ha funcionado bien ahora podemos hacer la prueba a cambiar el drbd a otro nodo y ver que los datos siguen allí:

1. nodo1

    umount /mnt

    drbdadm secondary drbd1

2. nodo2

    drbdadm primary drbd1

    mount /dev/drbd0 /mnt

El sistema de drbd tiene opciones para tomar decisiones en caso de un 'split-brain'. Es el caso en el que vuelve un nodo caido y se encuentran que los dos han avanzado en los datos. Para que esto no ocurra lo ideal es que haya siempre tres nodos o un gestor de clúster que tenga información de lo que está pasando y pueda hacer *fencing* a los nodos en caso de problemas.

Por lo tanto esta parte la abordaremos en el siguiente apartado, en el que pondremos el *Pacemaker* para gestionar nuestro clúster.