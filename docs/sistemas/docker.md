## Docker

### Instalando servicio docker

- [https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/)

Para ubuntu 22.04 LTS Jammy:

```
sudo apt-get remove docker docker-engine docker.io containerd runc curl
sudo apt-get update
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
sudo mkdir -m 0755 -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
sudo systemctl status docker
```

### Imágenes y contenedores

Las imágenes se generan con el Dockerfile. Son estáticas una vez hecho su *build*.
Los contenedores es la ejecución en *namespaces* de Linux de las imágenes.

Por lo tanto primero debemos construir una imágen y posteriormente podremos crear contenedores que ejecutaran el código de esa imágen. Podremos ejecutar tantos contenedores idénticos como queramos a partir de una imágen.

#### Imágenes: Dockerfile

Referencia: [https://docs.docker.com/engine/reference/builder/](https://docs.docker.com/engine/reference/builder/)

El fichero Dockerfile deberá contener un mínimo. Normalmente partiremos de una imágen base de una distribución, aunque podría ser ya de un aplicativo, es decir, estaremos heredando una imágen de la que partiremos para añadir cosas:

- FROM: imágen de la que partiremos

Y luego podremos añadir más órdenes a ejecutar durante el *build*:

- RUN: orden a ejecutar cuando el contenedor basado en esta imágen se 

Esta imágen puede ser una hecha por nosotros previamente, pero normalmente cogeremos una de un repositorio, como [https://hub.docker.com/](https://hub.docker.com/)

Por ejemplo, podemos partir de un Ubuntu. Miraremos los *tags* (identificadores de versión de la imágen) y partiremos de ella:

```
FROM ubuntu:latest
RUN echo "Este es mi ubuntu" > /miubuntu.txt
```

Y podremos construïr nuestra imágen. Siempre miraremos de ponerle un *tag* para que sea más fácil identificarla:

```docker build -t miubuntu .```

Veremos que la primera vez que partimos de una imágen y un tag se descargan una serie de segmentos. En realidad cada segmento es una instrucción que pusieron en ese Dockerfile al construirlo. Lo podemos ver en [https://hub.docker.com/layers/library/ubuntu/latest/images/sha256-c985bc3f77946b8e92c9a3648c6f31751a7dd972e06604785e47303f4ad47c4c?context=explore](https://hub.docker.com/layers/library/ubuntu/latest/images/sha256-c985bc3f77946b8e92c9a3648c6f31751a7dd972e06604785e47303f4ad47c4c?context=explore)

Podemos mirar las imágenes que tenemos con:

```docker image ls```

En dónde deberíamos tener como mínimo la *ubuntu:latest* y la *miubuntu* que hemos creado a partir de esa con nuestra orden RUN

#### Contenedores

Para crear un contenedor deberemos, como hemos comentado antes, ejecutar una imágen:

```
docker run miubuntu
```

No pasa nada. Esto es porque en realidad este contenedor no va a ejecutar nada cuando la iniciemos a partir de la imágen, porque no tiene ninguna orden en tiempo de ejecución para el docker. Podemos ver que el contenedor si se ha creado y ejecutado, pero que ha finalizado sin más:

```
docker ps --all
```

Para dar nombre a un contenedor:

```
docker run --name contenedor miubuntu
```

Si volvemos a mirar ahora veremos que hay dos contenedores, parados los dos.

Y para eliminarlo:

```
docker rm contenedor
```

Para que se pueda ejecutar algo deberemos decirle qué queremos que haga el contenedor:

```
docker run miubuntu cat /miubuntu.txt
```

Y veremos que dentro está nuestro fichero, muestra el contenido. Es el fichero que hemos creado en la fase de construcción de la imágen. Por lo tanto es algo *estático* que está dentro de la imágen.

Normalmente querremos que lo que queramos que haga el contenedor ya esté definido en la imágen. Para ello hemos de construir de nuevo la imágen indicando lo que debe hacer el contenedor que se cree a partir de ella:

```
FROM ubuntu:latest
RUN echo "Este es mi ubuntu" > /miubuntu.txt
CMD ["cat","/miubuntu.txt"]
```

Ahora lo hará sin necesidad de indicárselo:

```
docker run miubuntu
```

##### A tener en cuenta en los contenedores

1. Debemos añadir lo que necesitemos

    Las imágenes *base* de distribuciones suelen mirar de ser lo más pequeñas posibles, con lo cual faltan muchas órdenes que damos por supuesto.

    ```docker run miubuntu ip address show```
    ```docker run miubuntu bash -c "apt update;apt install iproute2 -y;ip address show"```

    Si ejecutamos de nuevo ```docker run miubuntu ip address show```, la órden ya no está. Es un contenedor nuevo cada vez! 
    Si queremos que esté siempre, qué deberemos hacer? Añadir a la imágen ```RUN apt update;apt install iproute2 -y``` y volver a hacer el *build*

2. Solo un proceso se ejecuta

    Sólo hay un único proceso con PID=1 que se ejecuta. Si muere, el contenedor finaliza. 
    ```docker run miubuntu ps auxf```.

3. Podemos ejecutar y entrar a la vez en un contenedor si ejecutamos un intérprete de ćomandos:

    ```docker run -ti miubuntu /bin/sh```

    Si en otro terminal miramos los contenedores que se están ejecutando lo veremos:```docker ps```
    También podremos entrar desde este terminal a ese contenedor que está ejecutándose en el otro terminal: ```docker exec -ti <nombre_contenedor> /bin/sh```

4. Si queremos ejecutar un servicio que estará iniciado siempre, como un servidor web, este deberá ser ejecutado con la opción de *daemonize* (-d) del docker run:

```
FROM ubuntu:latest
RUN apt update
RUN apt install -y python3 curl
CMD ["python3","-m","http.server"]
```

```
docker build -t pythonserver .
docker run -d --name pythonserver1 pythonserver
```

5. Podemos añadir y modificar tanto redes como volúmenes, que serán directorios que podemos compartir entre el servidor y el contenedor. Pero todo esto es muy engorroso sólo con docker, necesitamos un orquestrador!
