# Contenedores isardvdi

Dependiendo de la configuración establecida en *isardvdi.cfg* obtendremos un conjunto de contenedores:

```
# docker-compose ps
          Name                         Command                  State                                                          Ports                    
--------------------------------------------------------------------------------------------------------------------------------------------------------
isard-api                   /bin/sh -c /run.sh               Up (healthy)                                                                               
isard-authentication        /run.sh                          Up                                                                                         
isard-db                    rethinkdb --cores 64 --bin ...   Up (healthy)   28015/tcp, 29015/tcp, 8080/tcp                                              
isard-engine                /usr/bin/supervisord -c /e ...   Up (healthy)                                                                               
isard-grafana               /bin/sh -c /run_isard.sh & ...   Up             3000/tcp                                                                    
isard-grafana-agent         /run.sh                          Exit 127                                                                                   
isard-guac                  /guac                            Up             4567/tcp                                                                    
isard-hypervisor            /src/start.sh                    Up                                                                                         
isard-loki                  /usr/bin/loki -config.file ...   Up             3100/tcp                                                                    
isard-portal                docker-entrypoint.sh hapro ...   Up             0.0.0.0:443->443/tcp,:::443->443/tcp, 0.0.0.0:80->80/tcp,:::80->80/tcp,     
                                                                            0.0.0.0:9999->9999/tcp,:::9999->9999/tcp                                    
isard-prometheus            /bin/prometheus --config.f ...   Up             9090/tcp                                                                    
isard-scheduler             python3 start.py                 Up (healthy)                                                                               
isard-squid                 /bin/sh /run.sh                  Up                                                                                         
isard-static                /docker-entrypoint.sh ngin ...   Up             80/tcp                                                                      
isard-stats-cadvisor        /usr/bin/cadvisor -logtost ...   Up (healthy)   8080/tcp                                                                    
isard-stats-go              /run.sh                          Up                                                                                          
isard-stats-node-exporter   /bin/node_exporter --path. ...   Up                                                                                          
isard-stats-rethinkdb       /rethinkdb-prometheus-expo ...   Up                                                                                          
isard-storage               /init.sh                         Up                                                                                           
isard-vpn                   sh run.sh                        Up             0.0.0.0:443->443/udp,:::443->443/udp, 0.0.0.0:4443->4443/udp,:::4443->4443/udp
isard-webapp                python3 -u start.py              Up (healthy)   5000/tcp                                                                      
isard-websockify            /websockify                      Up  
```

![Esquema IsardVDI](img/isard-schema.jpg)

El sistema constará de partes principales:

1. Entrada y salida del sistema

    - **isard-portal**: haproxy balanceador
    - **isard-vpn**: python3 Wireguard server. Incorpora ahora el *guacd* de guacamole para el RDP Browser

2. Servidores web y api

    - **isard-static**: Servidor web nginx con el compilado de Vue de *isard-frontend* y con fotos estáticas y libreria js noVNC.
    - **isard-webapp**: Servidor python3 Flask con la parte html/js de administración.
    - **isard-api**: Servidor python3 Flask con la *API REST* principal del sistema.

3. Autenticación

    - **isard-authentication**: Contenedor GoLang con los diferentes sistema de authenticación: local (rethinkdb), OAuth2 (Google/Gitlab), OpenLdap (Ldap/AD) y SAML

4. Orquestrador y BBDD

    - **isard-engine**: Python3 multithreading que gestiona y controla los hypervisores y las acciones y eventos de los dominios y los discos.
    - **isard-db**: Base de datos documental RethinkDB

5. Hypervisor y almacenamiento

    - **isard-hypervisor**: Contenedor con los servicios de *libvirtd* y ejecutables qemu para la virtualización de hardware.
    - **isard-storage**: Contenedor Fedora con los paquetes de *libguestfs* para manejar discos *qcow2*

6. Proxies de video

    - **isard-squid**: Proxy squid que gestiona la conexión de túnel TLS a través del puerto 80 hacía el hypervisor correspondiente.
    - **isard-websockify**: Proxy GoLang de protocolo VNC hacía el hypervisor correspondiente.

7. Recolectores de estadísticas

    - **isard-stats-cadvisor**: Extrae estadísticas de los contenedores de docker que haya arrancados en el servidor.
    - **isard-stats-go**: Tiene *collectors* y extrae estadísticas personalizadas según el *FLAVOUR* que esté arrancado.
    - **isard-stats-node-exporter**: Extrae métricas básicas del servidor.
    - **isard-stats-rethinkdb**: Extrae estadísticas de rethink.
    - **isard-grafana-agent**: Centraliza la exportación de estadísticas y logs de los diferentes contenedores anteriores, que tienen un endpoint HTTP. Extrae también los logs de los contenedores que se están ejecutando. Estos logs están en */var/lib/docker*.

8. Monitorización estadísticas y alertas

    - **isard-prometheus**: Es la BBDD de tiempos en dónde se reciben las estadísticas recogidas por el *isard-grafana-agent*
    - **isard-grafana**: Es en dónde podemos mostrar en gráficos los datos de Prometheus.
    - **isard-loki**: Es una BBDD de logs.

Y algun contenedor más, como **isard-scheduler** para programar tareas.


