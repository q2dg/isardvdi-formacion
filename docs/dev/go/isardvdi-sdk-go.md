# `isardvdi-sdk-go`

`isardvdi-sdk-go` (IsardVDI Software Development Kit Go) es una librería escrita en Go que utilizan todos los servicios escritos en Go que necesitan hacer llamadas a la API.

Básicamente, nos ahorra tener que escribir cada petición "a mano", previene errores y duplicación de código y nos ofrece un `mock`. Este sirve para escribir tests, ya que, en vez de llamar a la API real, le especificamos nosotros qué es lo que responde. De esta forma, podemos escribir tests sin depender de un servidor y podemos hacer que "falle" de formas específicas y comprobar que nuestro código reaccione correctamente.

[Consultar documentación](https://pkg.go.dev/gitlab.com/isard/isardvdi-sdk-go)
