# Preparar el entorno para el desarrollo

## Crear una copia local - git clone

Una vez realizada una copia del proyecto de IsardVDI se puede modificar su código fuente. Para ello, primero se debe descargar una copia local del proyecto, mediante el comando:

* Comando con configuración ssh: `git clone git@gitlab.com:${my_gitlab_user}/isardvdi.git`

* Comando con configuración https: `https://gitlab.com/${my_gitlab_user}/isardvdi.git`

!!! Tip "Consejo"
    Se puede obtener la url mediante la web de gitlab 

    ![](../img/git/ssh_https.png)

En este momento el esquema es el siguiente:

* En Gitlab existen el [proyecto oficial](https://gitlab.com/isard/isardvdi) y su copia (*fork*).
* La copia se ha descargado de manera local, por lo que está relacionada con su origen, que se puede compobar mediante el comando `git remote -v`.

![](../img/git/isard_workflow2.png)

El siguiente paso sería relacionar esta copia local con el repositorio oficial remoto. Se puede conseguir mediante el siguiente comando:

`git remote add upstream git@gitlab.com:isard/isardvdi.git`

![](../img/git/isard_workflow3.png)

!!! Danger "Aclaración"
    Mantendremos el repositorio local relacionado con el repositorio remoto oficial para mantenerlo actualizado.

## Encapsular código - git branch

Git se organiza en ramas (branches), que tienen como finalidad encapsular los cambios en el código. Tendiendo en mente que [git funciona como una secuencia de copias](intro.md#cómo-funciona) podemos definir una rama como un recopilador de copias, a las que llamaremos [*commit*](add_commit_push_mr.md#guardar-cambios---git-commit).

Por defecto el repositorio se crea con una única rama **main** (rama principal), pero se pueden crear tantas ramas como sea necesario.

![Branch](../img/git/git_branch.png)

El gráfico anterior es un ejemplo de un repositorio con dos ramas (rama 1 y rama 2) en las que en cada una se han realizado dos copias (commit 1 y commit 2).

Para ver en qué rama se está se debe utilizar el comando `git branch` y para ver la historia de la rama `git log --graph`.

## Mantener el repositorio actualizado con git pull o git rebase

Es importante tener en mente que el clon local realizado sobre el proyecto oficial no se actualizará automáticamente, ya que al crearlo **este se convierte en independiente**.

Podemos mantener el código actualizado de varias maneras, pero nos centraremos en dos. 

!!! Warning "Atención"
    Aunque en este caso ambas opciones cumplen el mismo propósito la manera de integrar los cambios es diferente, más información en: [https://git-scm.com/book/en/v2/Git-Branching-Rebasing](https://git-scm.com/book/en/v2/Git-Branching-Rebasing)

**Actualización - git fetch y git rebase**

Antes de empezar una rama y realizar cambios es recomendable comprobar si hay alguna modificación en alguno de los repositorios remotos frecuentemente mediante `git fetch ${remote}` (o `git fetch --all`) .

`git fetch --all`

Esta acción no realiza ninguna transferencia de archivos, por lo que debe combinarse con otros comandos como `git rebase ${[remote/]rama}` para actualizar el código. Uno de los comandos utilizados es git rebase que integra los cambios de una rama en otra. Por ejemplo al realizar el siguiente comando:

`git rebase upstream/main`

Al estar en la rama main de nuestro repositorio local estaremos actualizandola integrando los cambios que hayan en la rama main del repositorio remoto.

**Actualización mediante git pull**

Otra opción sería git pull, que sería la combinatoria de `git fetch` y `git merge`. Primero actualiza el repositorio remoto y después incorpora los cambios en la rama local.

`git pull`

## Empezar a trabajar en una rama

Para desarrollar hay que crear una rama (branch) mediante uno de los siguientes comandos:

**Crear una rama en base a la rama principal del repositorio remoto oficial**. Esto implica que el código estará actualizado: `git switch -C my_feature upstream/main`.

**Crear una rama en base a la rama actual**. Hay que considerar que si la rama en la que se está situado no está actualizada tampoco lo estará la que creemos: `git checkout -b my_feature`.

Una vez creada una rama ya se pueden realizar cambios sobre el código.
