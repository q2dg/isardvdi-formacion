# Sistema de control de versiones

## Qué es?

Es una herramienta que permite hacer un seguimiento sobre los cambios realizados sobre el código fuente.

## Que nos permite?

**Colaborar de manera eficiente**. Los programadores pueden realizar modificaciones sobre diferentes partes del código en paralelo y fusionarlos de manera sencilla.

**Comparar versiones del código**. Puede compararse el contenido del código y ver las diferencias entre dos momentos determinados.

**Recuperar versiones pasadas**. Los programadores tienen acceso a un historial completo del código, por lo que pueden retroceder a cualquier punto anterior en el tiempo.

Lectura recomendada: [https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones)

## Cómo funciona?

Cada vez que confirmas un cambio, se toma una foto del aspecto de todos tus archivos en ese momento y se guarda una referencia a esa copia. Si los archivos no se han modificado no almacena el archivo de nuevo, sino un enlace al archivo anterior idéntico que ya tiene almacenado. Es decir, los datos se almacenan como una secuencia de copias.

![Ejemplo de git](../img/git/snapshots.png)

Lectura recomendada: [https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)

## Instalación

Fedora:

`$ yum install git`

Distribuciones basadas en Debian:

`$ apt-get install git`

Más información: [https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Instalaci%C3%B3n-de-Git](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Instalaci%C3%B3n-de-Git)


## Gitlab

Una de las plataformas más utilizadas para el control de versiones es [Gitlab](https://about.gitlab.com/), que permite la gestión del código mediante una interfaz web.

* [Crear una cuenta](https://gitlab.com/users/sign_up)
* [Proyecto oficial](https://gitlab.com/isard/isardvdi)
    * **Crear issues**. Colaborar con ideas, preguntas, pedir asistencia, etc.
![](../img/git/create_issue1.png)
![](../img/git/create_issue2.png)

## Desarrollo sobre IsardVDI

El funcionamiento será el siguiente:

1. Se creará una copia del proyecto de la cual seremos propietarios ([fork](#hacer-una-copia-de-un-proyecto-para-desarrollar)).
2. Descargaremos nuestra copia del proyecto para modificar su código ([clone](clone_fetch_pull.md#git-clone)).

Nos referiremos a estos proyectos como **repositorios**.

![](../img/git/isard_workflow1.png)

## Hacer una copia de un proyecto para desarrollar

Para poder desarrollar sobre IsardVDI debemos realizar primero una copia del proyecto. Mediante la web de gitlab creamos un *fork* del [proyecto oficial](https://gitlab.com/isard/isardvdi):

![Fork de un proyecto git](../img/git/fork.png)
