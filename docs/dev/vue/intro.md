# Qué es VueJS
VueJs es un framework progresivo para el desarrollo de interfaces de usuario. Está diseñado para permitirnos programar mediante un enfoque basado en componentes, por lo que podremos tener bloques de código reutilizables. En IsardVDI lo utilizamos para lo que llamamos frontend, que es el apartado con el que interactua el usuario.

## Cómo se utiliza en IsardVDI

!!! Info "Recordatorio"

    El modo de ejecución del proyecto depende de lo que tengamos definido en la variable USAGE del fichero isardvdi.cfg. Para el desarrollo debe ser "devel".

El frontend de Isard se ejecuta en el container isard-frontend-dev (en el caso de desarrollo) o en el container isard-static (en el resto de casos). Veamos el Dockerfile y cómo cambia el docker-compose en base al usage definido para ver la diferencia entre ambos:

**Production**

El DockerFile de producción está dividido en dos ([multi-stage build](https://docs.docker.com/build/building/multi-stage/)):

1. Creamos un artefacto preparado para producción.
2. Servimos el artefacto creado mediante Nginx.

```
# 1. Creamos un artefacto preparado para producción.
FROM node:18-alpine as frontend

# Creamos el directorio isard-frontend dentro del container y metemos el código
RUN mkdir /isard-frontend
COPY frontend /isard-frontend

# Añadimos la versión en el login
ARG SRC_VERSION_ID
ARG SRC_VERSION_LINK
RUN sed -i "s*isard_display_version*${SRC_VERSION_ID}*g" /isard-frontend/src/views/Login.vue
RUN sed -i "s,isard_changelog_link,${SRC_VERSION_LINK},g"  /isard-frontend/src/views/Login.vue

# Vamos al directorio del código
WORKDIR /isard-frontend

# Instalamos todas las dependencias
RUN apk add python3 make g++
RUN yarn
RUN yarn build

RUN rm -rf src
RUN rm -rf node_modules
RUN rm -rf build

# 2. Servimos el artefacto creado mediante Nginx.
FROM nginx:alpine as production

COPY docker/static/default.conf /etc/nginx/conf.d/default.conf
COPY docker/static/spice-web-client /usr/share/nginx/html/viewer/spice-web-client
COPY docker/static/noVNC /usr/share/nginx/html/viewer/noVNC
COPY frontend/src/assets/logo.svg /usr/share/nginx/html/default_logo.svg
COPY --from=frontend /isard-frontend/dist /usr/share/nginx/html
```

En resumen se copia el código de frontend dentro de un container basado en alpine donde creamos el artefacto y lo servimos mediante un Nginx. Esto quiere decir que tenemos una versión fija del frontend, una vez el código se ha copiado y se ha creado el artefacto resultan indiferentes los posteriores cambios en el código porque lo que recibiremos del Nginx siempre será lo mismo.

**Devel**

En el caso del modo devel tenemos que centrarnos en el fichero docker-compose.yml. No nos hace falta ningún Dockerfile ya que podemos utilizar directamente la imagen de docker de node y mediante los comandos yarn install && yarn serve ejecutamos el frontend con hot reaload (se actualiza al detectar modificaciones):

```
  isard-frontend-dev:
    command:
    - sh
    - -c
    - yarn install && yarn serve
    container_name: isard-frontend-dev
    environment:
      CYPRESS_CACHE_FOLDER: /usr/local/src/isard-frontend/node_modules/.cache/cypress
    image: node:18
    logging:
      driver: json-file
      options:
        max-size: 100m
    networks:
      isard-network: null
    volumes:
    - type: bind
      source: ./frontend/
      target: /usr/local/src/isard-frontend
      bind:
        create_host_path: true
    working_dir: /usr/local/src/isard-frontend/
```

!!! Info "Información avanzada"
    En los dockers de frontend utilizamos los comandos yarn build y yarn serve, para saber más en detalle qué hacen estos comandos podemos ver su definición en el fichero package.json de frontend (en scripts):
    ```
        "name": "isard-frontend",
        "version": "0.1.0",
        "private": true,
        "scripts": {
            "serve": "NODE_OPTIONS=--openssl-legacy-provider vue-cli-service serve",
            "build": "NODE_OPTIONS=--openssl-legacy-provider vue-cli-service build",
            "test:e2e": "vue-cli-service test:e2e",
            "lint": "vue-cli-service lint",
            "i18n:report": "vue-cli-service i18n:report --src './src/**/*.?(js|vue)' --locales './src/locales/**/*.json'",
            "test": "jest src"
        },
        .
        ..

    ```

## Estructura de frontend

A continuación nos centraremos en la estructura de directorios de frontend, centrandonos en algunos directorios y ficheros concretos.

![Archivos de frontend](../img/vue/frontend_files.png)

**node_modules**

Es el almacén de paquetes que utiliza NodeJS a través de NPM y donde se guardan todas las dependencias de nuestro proyecto.

**public**

Contiene ficheros estáticos que no se procesarán con webpack. Webpack se encarga de crear módulos del código, permitiendo así la comunicación entre sus dependencias. Es decir, se encarga de estructurar el proyecto para que se puedan reutilizar los componentes sin que haya sobreescritura de variables o conflictos entre los módulos incorporados en una página.

**src**

Contiene el código fuente del apartado de frontend.

* assets: Contiene las imágenes y estilos utilizados.
* components: Contiene los segmentos de código con los que generamos las páginas.
* layouts: Contiene diferentes estructuras de la vista.
* lib: contiene los archivos javascript para el funcionamiento del visor RDP en el navegador.
* locales: contiene los archivos de traducción.
* pages: Contiene las páginas a las cuales tenemos acceso una vez logueados.
* router: Se encarga de la asociación de cada ruta con la página correspondiente.
* shared: Contiene variables constantes utilizadas en diferentes vistas.
* store: Contiene la "store" de la aplicación. Entendemos como "store" al lugar donde almacenamos la información que está visualizando el usuario. Esto nos permite tener un único lugar centralizado al que referirnos y en el que podremos compartir información entre los diferentes componentes que utilicemos.
* utils: Contiene funciones de formatación de los datos.
* views: Contiene acceso a las páginas que no requieren autenticación (por ejemplo, paginas de error, el visor directo, la página de registro, etc).

**package.json**

Contiene la configuración del proyecto (plugins utilizados, tipos de ejecución del proyecto, etc). Las dependencias definidas aquí son las que estarán posteriormente en node_modules.

## Vue router
Se encarga de la asociación de cada ruta con la página correspondiente. Por ejemplo para la página de profile vemos que si nos dirigimos a la ruta /profile está asociado al component MainLayout, que es el que incorpora la estructura de la página con la barra de navegación. Vemos también que tiene una ruta hija, que es la que contiene el component de Profile. En meta tiene definido el título que se mostrará en la pestaña del navegador (hacemos referencia a la traducción) y en allowedRoles los roles que pueden acceder a dicho apartado. 

```
{
  path: '/deployments',
  component: MainLayout,
  children: [
    {
      path: '',
      name: 'deployments',
      component: Deployments,
      meta: {
        title: i18n.t('router.titles.deployment'),
        allowedRoles: ['admin', 'manager', 'advanced']
      }
    },
    {
      path: 'new', // Realmente nos lleva a /deployments/new
      name: 'deploymentsnew',
      component: DeploymentNew,
      meta: {
        title: i18n.t('router.titles.new_deployment'),
        allowedRoles: ['admin', 'manager', 'advanced']
      }
    }
  ],
  meta: {
    requiresAuth: true
  }
},
{
  path: '/deployment/:id',
  component: MainLayout,
  children: [
    {
      path: '',
      name: 'deployment_desktops',
      component: Deployment,
      meta: {
        title: i18n.t('router.titles.deployment'),
        allowedRoles: ['admin', 'manager', 'advanced']
      }
    },
    {
      path: 'videowall', // Realmente nos lleva a /deployment/:id/videowall
      name: 'deployment_videowall',
      component: DeploymentVideowall,
      meta: {
        title: i18n.t('router.titles.deployment_videowall'),
        allowedRoles: ['admin', 'manager', 'advanced']
      }
    }
  ],
  meta: {
    requiresAuth: true
  }
},
{
  path: '/profile',
  component: MainLayout,
  children: [
    {
      path: '',
      name: 'profile',
      component: Profile,
      meta: {
        title: i18n.t('router.titles.profile'),
        allowedRoles: ['admin', 'manager', 'advanced', 'user']
      }
    }
  ],
  meta: {
    requiresAuth: true
  }
},
```

Más información en: [https://router.vuejs.org/guide/essentials/nested-routes.html](https://router.vuejs.org/guide/essentials/nested-routes.html)

## Locales

Contiene los archivos de traducciones. Las traducciones se gestionan mediante [Weblate](https://hosted.weblate.org/projects/isardvdi/frontend/) pero en el caso de las nuevas funcionalidades se crean directamente las traducciones correspondientes en inglés, castellano y catalán. Están definidas en ficheros json llenos de diccionarios a los que referenciaremos desde cada componente. Miremos por ejemplo la traducción de la redirección al panel de administración:

Euskera
```
{
    "components": {
        "navbar": {
            "admin": "Administrazioa",
            .
            .
            .
        },
        .
        .
        .
```

Catalán

```
{
    "components": {
        .
        .
        .
        "navbar": {
            "admin": "Administració"
            .
            .
            .
        },
```

Castellano

```
{
    "components": {
        .
        .
        .
        "navbar": {
            "admin": "Administración"
            .
            .
            .
        },
```

Las claves deben coincidir para poderlo referenciar desde la vista. Podemos buscar donde se referencia, en este caso estará como "components.navbar.admin":

```
<b-nav-item
    v-if="['admin', 'manager'].includes(getUser.role_id)"
    @click="loginAdmin()"
>
    <font-awesome-icon
    :icon="['fas', 'user-cog']"
    class="mr-1 d-lg-none d-xl-inline"
    />
    {{ $t("components.navbar.admin") }}
</b-nav-item>
```

## Herramientas utilizadas

Para esta formación utilizaremos:

- Editor de código [VSCode](https://code.visualstudio.com/)
- Extensión Vue.js devtools de [Firefox](https://addons.mozilla.org/en-US/firefox/addon/vue-js-devtools/) o [Chrome](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd)
- [git](../git/intro.md) para hacer el control de versiones