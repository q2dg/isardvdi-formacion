# Gráficas

1. Para crear un nuevo panel, en el apartado de `Dashboards` ir a `New Dashoboard`.

![](https://i.imgur.com/CYdDpFd.png)

1.1 También se puede crear yendo al apartado de `Dashboards` y con el botón `New Dashboard`.

![](https://i.imgur.com/e28duN1.png)

1.2 Otra forma sería en el apartado `Explorer`, añadir directamente un nuevo panel con la query o añadir la query a paneles ya existentes:

![](https://i.imgur.com/q06ReVA.png)

Se selecciona la BBDD de donde se quiera extraer los logs, en este caso de `Loki`:

![](https://i.imgur.com/nBVQ6wr.png)

Esta página consta de diferentes opciones, para más información https://grafana.com/docs/grafana/latest/explore/:

![](https://i.imgur.com/2lalbTs.png)

Ir al botón `Label browser`, donde aparecerán las etiquetas que se extraen con Loki, ***container_id, container_name, domain...***

![](https://i.imgur.com/uCLp0Od.png)

Seleccionar un label:

![](https://i.imgur.com/XzCZd2r.png)

Elegir los contenedores que se desee para extraer los logs:

![](https://i.imgur.com/T9qkQG7.png)

Al seleccionar los dos contenedores aparecerán los logs de ambos:

![](https://i.imgur.com/NpYd7ix.png)

Se puede filtrar la hora para poder ver los logs de cierto tiempo:

![](https://i.imgur.com/g9H6shO.png)

Se puede añadir la recarga automática de la página para cada vez que se cambie la query se actualice:

![](https://i.imgur.com/pTDKAxh.png)

En el `Explorer` hay opciones de ayuda para crear Queries, como por ejemplo el modo `Builder`, también existe el botón de `Explain query` que te dará información sobre la query que estás creando, otro apartado importante es el `Kick start your query`, que te muestra ejemplos de Queries que pueden ser útiles:

![](https://i.imgur.com/3qjQgyF.png)

***Ejemplo de query para poder ver los logs de los contenedores api y engine y filtrando la palabra Error:***
```
{container_name=~"isard-api|isard-engine"} |= "Error"
```

Al terminar de elegir la Query correcta, se crea el panel nuevo yendo a `Add to dashboard:`

![](https://i.imgur.com/yee10BZ.png)

Se puede seleccionar un Panel que ya esté creado o crear uno nuevo:

![](https://i.imgur.com/5i8WdsF.png)

2. Añadir un panel en blanco con **Add a new panel**:

![](https://i.imgur.com/9EGV0bq.png)

>En Prometheus, los datos se organizan utilizando una estructura jerárquica basada en etiquetas (**tags**). Cada métrica se identifica mediante un conjunto de etiquetas que proporcionan información adicional sobre la métrica en cuestión. Las etiquetas son pares de **clave-valor** que se asocian con cada punto de datos en una serie temporal, en el siguiente ejemplo se muestra como se extraen datos de usuarios, escritorios y tráfico de los puertos de visores del contenedor isard-api.

***Ejemplo de Query para poder comprobar usuarios, escritorios, grupos y categoría con Visor:***

```
label_replace(isardvdi_isardvdi_api_user_info{}, "user", "$1", "id", "(.*)") * on (user) group_right (category, group) label_replace(isardvdi_isardvdi_api_desktop_info{} * on (id) group_left(hypervisor) label_replace(

    isardvdi_domain_port_vnc{hypervisor="bm-e2-02.pilotfp.gencat.isardvdi.com"}
        * on(desktop) group_left isardvdi_domain_port_vnc{hypervisor="bm-e2-02.pilotfp.gencat.isardvdi.com"}
        * on (port, hypervisor) group_left isardvdi_socket_recv_bytes{hypervisor="bm-e2-02.pilotfp.gencat.isardvdi.com"},

    "id", "$1", "desktop", "(.*)"), "desktop", "$1", "id", "(.*)")
```

Veamos paso a paso cómo funciona:

- `label_replace(isardvdi_isardvdi_api_user_info{}, "user", "$1", "id", "(.*)")`
Esta parte de la consulta utiliza la función **label_replace** para reemplazar la etiqueta "user" en la métrica ***isardvdi_isardvdi_api_user_info*** con el valor extraído de la etiqueta "id". La expresión "(.*)", entre paréntesis, captura el valor de "id", esta métrica extrae datos de Isard api que nos da información sobre los usuarios que hay. Esto significa que se tomará el valor de "id" y se reemplazará en la etiqueta "user".

>Datos extraídos con la métrica isardvdi_isardvdi_api_user_info:
![](https://i.imgur.com/xrOsN6h.png)

- `* on (user) group_right (category, group)`
Aquí, la parte `* on (user)` indica que los datos se agruparán por la etiqueta "user". La parte **group_right** indica que la agrupación se realizará en el lado derecho. Además, se agruparán también por las etiquetas "category" y "group" en el lado derecho.

- `label_replace(isardvdi_isardvdi_api_desktop_info{} * on (id) group_left(hypervisor), "id", "$1", "desktop", "(.*)")`
En esta parte de la consulta, se utiliza nuevamente la función **label_replace** para reemplazar la etiqueta "desktop" en la métrica ***isardvdi_isardvdi_api_desktop_info*** con el valor extraído de la etiqueta "id". La expresión "(.*)", entre paréntesis, captura el valor de "id". Posteriormente, se realiza una agrupación por la etiqueta "hypervisor" en el lado izquierdo.

>Datos extraídos con la métrica isardvdi_isardvdi_api_desktop_info:
![](https://i.imgur.com/9j3368X.png)

- Por último:
    ```
    isardvdi_domain_port_vnc{hypervisor="bm-e2-02.pilotfp.gencat.isardvdi.com"}
        * on(desktop) group_left isardvdi_domain_port_vnc{hypervisor="bm-e2-02.pilotfp.gencat.isardvdi.com"}
        * on (port, hypervisor) group_left isardvdi_socket_recv_bytes{hypervisor="bm-e2-02.pilotfp.gencat.isardvdi.com"},

    "id", "$1", "desktop", "(.*)"), "desktop", "$1", "id", "(.*)")
    ```
    Aquí, se realiza otra operación de **label_replace** en la métrica ***isardvdi_domain_port_vnc***. Se reemplaza la etiqueta "desktop" con el valor extraído de la etiqueta "id" usando la expresión "(.*)". Luego, se realiza una agrupación por las etiquetas "port", "hypervisor" en el lado izquierdo y se agrega la métrica ***isardvdi_socket_recv_bytes*** con la etiqueta "hypervisor" específica.

>Datos extraídos con la métrica isardvdi_domain_port_vnc:
![](https://i.imgur.com/RD70ZYk.png)

>Datos extraídos con la métrica isardvdi_socket_recv_bytes:
![](https://i.imgur.com/aeFjT6d.png)

Este ejemplo es para el visor VNC (HTML5/Navegador). Para los otros visores se tiene que cambiar la parte central por lo siguiente:

SPICE:
```
isardvdi_domain_port_spice_tls{hypervisor="bm-e2-02.pilotfp.gencat.isardvdi.com"} * on(desktop) group_left isardvdi_domain_port_spice_tls{hypervisor="bm-e2-02.pilotfp.gencat.isardvdi.com"} * on (port, hypervisor) group_left isardvdi_socket_recv_bytes{hypervisor="bm-e2-02.pilotfp.gencat.isardvdi.com"}
```

RDP:
```
isardvdi_domain_net_rx_bytes{hypervisor="bm-e2-02.pilotfp.gencat.isardvdi.com"} * on (mac) group_left isardvdi_conntrack_rdp_recv_bytes{hypervisor="bm-e2-02.pilotfp.gencat.isardvdi.com"}
```

![](https://i.imgur.com/UCEwxeu.png)

En este apartado hay diferentes opciones:

- Se puede cambiar el tipo de panel que se desee:

    ![](https://i.imgur.com/gAyjxMa.png)

    ![](https://i.imgur.com/1h7ozyB.png)

    ![](https://i.imgur.com/clraM70.png)

    ![](https://i.imgur.com/MoKM6hQ.png)

Y por otra parte, se puede personalizar el panel:

- Cambiar el nombre del panel:

    ![](https://i.imgur.com/INxYDNA.png)

    ![](https://i.imgur.com/nSVIpEP.png)

- Para hacer visible la leyenda de la gráfica o no, modificar para que esté en modo tabla o lista, se puede cambiar de lado la leyenda y añadir el último valor, el primero, el mínimo de la gráfica en la leyenda del panel:

    ![](https://i.imgur.com/umoQaxR.png)

    ![](https://i.imgur.com/yFd9s0Z.png)
 
- También se puede cambiar el color de las líneas en la gráfica:

    ![](https://i.imgur.com/iM3FZB0.png)

    ![](https://i.imgur.com/dsjEnr6.png)

!!! Info "Más Información"
    <https://grafana.com/docs/grafana/latest/dashboards/build-dashboards/modify-dashboard-settings/>

Para guardar este nuevo Panel, hay que ir al botón de configuración:

![](https://i.imgur.com/5VLkC6W.png)

Dentro del apartado de configuración, se encuentra el `JSON Model`:

![](https://i.imgur.com/e3LLjSW.png)

Hay que copiar este JSON:

![](https://i.imgur.com/POK9giC.png)

Por último, hay que ir al terminal de nuestra máquina en el path donde hayamos instalado el Isard, en el directorio `docker/grafana/dashboards`, crear un archivo .json con lo copiado anteriormente.

!!! Danger "Recrear el contenedor"
    Ahora, al arrancar cada vez el grafana ya contará con el nuevo panel creado.
    