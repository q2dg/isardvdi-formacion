# Alertas

![](https://i.imgur.com/2E7WDqB.png)

!!! Learn More "Referencia"
    <https://grafana.com/docs/grafana/latest/alerting/*>

!!! Info "Documentación"
    <https://grafana.com/docs/grafana/latest/alerting/set-up/provision-alerting-resources/file-provisioning/>

### Qué son?
Las **Alert rules** de Grafana te permiten establecer condiciones personalizadas para tus gráficos y paneles. Por ejemplo, puedes configurar una alerta para que te avise cuando la temperatura de tu servidor supere cierto umbral crítico. O quizás quieras que te notifiquen si el tráfico de tu sitio web se dispara repentinamente.

Los **Contact points** son los métodos de comunicación que Grafana utiliza para enviarte las alertas, como correo electrónico, Telegram...

Las **Notification policies** son reglas que determinan quién debe recibir las alertas y cómo se deben enviar. Puedes personalizar las notificaciones y asignar contactos específicos a cada política. 

## Ejemplos alertas
1. Para crear una alerta, hay que ir al apartado de `Alerting`, dentro de `Alert rules` y crear una nueva alerta en ![](https://i.imgur.com/L0vPrik.png) :

    ![](https://i.imgur.com/oflVtUS.png)


### Crear alerta por mensaje loki:
```
count_over_time({container_name="isard-engine"}  |= "'dead': ['disk_operations" [1m])
```

- Se selecciona el data source, en este caso **Loki**, para crear una alerta por mensaje de loki hay que seleccionar primero el contenedor donde aparezca el mensaje que queremos usar para crear la alerta, en este caso **isard-engine**, añadimos un **count_over_time** que hará un escaneo cada 1m de los logs de ese contenedor a ver si salta el mensaje que con un **Line contains** le indicamos que mensaje debería salir y alertar, esto se puede añadir manualmente o con ayuda del **Builder** de grafana:

    ![](https://i.imgur.com/xWKiVuO.png)

- Una vez hecho la query, seleccionamos las condiciones para la alerta sobre la query A y que esté por encima de 0, es decir que cada vez que salte el log del mensaje que queramos saltará la alerta:

    ![](https://i.imgur.com/TPGggON.png)

- En este apartado seleccionamos los tiempos de la alerta, el primer tiempo es para elegir cada cuanto se evalúa esta alerta y el segundo tiempo es una vez detecta la alerta y se queda en pending cuanto tarda en saltar en firing la alerta, es decir, en este caso sería: evalúa cada 30s y envía la alerta 30s después de detectarlo, en el siguiente apartado es para configurar los estados de la alerta, en primer lugar se selecciona que la alerta no cantará si no tiene ningún valor o es null el resultado, la segunda parte es para que al saltar la alerta lo marque como error:

    ![](https://i.imgur.com/HN25GP6.png)

- Este apartado sirve para darle un nombre a la alerta, crear o seleccionar la carpeta donde vayan las alertas y darle el nombre de grupo correspondiente para separar las alertas y darles diferentes configuraciones a la hora de lanzar las alertas como por ejemplo el tiempo:

    ![](https://i.imgur.com/NXSBygJ.png)

- Hay apartados opcionales como el `Summary and annotations`, estos apartados sirven para dar información extra o personalizada de lo que queramos mostrar en la alerta, en `Notifications` se pueden añadir **Labels** extra que aparezcan una vez tenga un valor determinado:
    ![](clipboard-202303061925-vfu4j.png)

### Crear alerta por superación de valor prometheus:
Es parecido al mensaje de loki, pero en este caso es para calcular la RAM en %, este cálculo se extrae de un contenedor llamado node-exporter, con esto podemos calcular la RAM y así poder hacer que cante una alterta cuando supera X % o valor:
```
100 - ((node_memory_MemAvailable_bytes{} * 100) / node_memory_MemTotal_bytes{})
```

![](https://i.imgur.com/w77dfqT.png)

- En este caso cantará la alerta cada vez que el % de la RAM supere el 80%:

    ![](https://i.imgur.com/9dYGk9S.png)

- Se añaden los campos con los valores que se desee:

    ![](https://i.imgur.com/K6psrxb.png)


## Exportar/Importar Alertas

1. Para extraer alertas que ya existen, primero generar un token temporal con los permisos de administrador, ir al grafana (/org/apikeys):

    ![](https://i.imgur.com/ux9kMGc.png) 

- Crear un nuevo token con role `Admin`:

    ![](https://i.imgur.com/RtLgtwS.png)

    ![](https://i.imgur.com/15EWRf6.png)

- Copiar el token:
  
    ![](https://i.imgur.com/JfYFTfh.png)

2. Coger el identificador de la alerta: 

    ![](https://i.imgur.com/wI4EhUR.png)

    ![](https://i.imgur.com/eIGshY3.png)


3. Lanzar el comando dentro del servidor donde esté corriendo el Grafana y así extraer el json, donde se remplaza con el `Token` de Grafana, el `Dominio` donde está arrancado y el `ID` de la alerta:
    ```
    curl -H "Authorization: Bearer <TOKEN>" <DOMAIN>/api/v1/provisioning/alert-rules/<ALERT ID> | jq`
    ```
!!! Warning "Alertas persistentes"
    Este json posteriormente se guarda en el **PATH** donde se haya montado el isard `/opt/isard/monitor/grafana/custom/alerting`, así cada vez que arranque el Grafana tendrá las alertas ya cargadas y sin tener que volver a ponerlas a mano.

## Conectar: Alertas de Grafana + TelegramBot
### Añadir un contact point

1. En el menú de Grafana, haz clic en el ícono de `Alertas` (campana) para abrir la página de Alertas que muestra las alertas existentes.

2. Haz clic en `Contact Point` para abrir la página que muestra los puntos de contacto existentes.

3. Haz clic en `New Contact Point`.

4. En el menú desplegable Alertmanager, selecciona un Alertmanager. Por defecto, se selecciona **Grafana Alertmanager**.

5. En **Nombre**, ingresa un nombre descriptivo para el contact point.

6. En **Tipo de Contact Point**, se selecciona el tipo y rellenas los campos obligatorios. Por ejemplo, si eliges correo electrónico, introduce las direcciones de correo electrónico o si eliges Telegram introduce el Token del Bot y el Chat ID.

7. Algunos tipos de Contact Point, como correo electrónico o webhook, tienen configuraciones opcionales. En **Configuraciones opcionales**, se especifica configuraciones adicionales para el tipo de punto de contacto que hayas puesto.

8. En **Configuraciones de notificación**, opcionalmente puedes seleccionar `Desactivar mensaje resuelto` si no quieres recibir una notificación cuando una alerta se resuelva.

9.  Haz clic en **Save contact point** para guardar los cambios de los contact points añadidos anteriormente.

## Crear un Contact Point de Telegram

![](https://i.imgur.com/4KECvVs.png)

Necesitarás un token de API BOT y un CHAT ID.

1. Abre Telegram y crea un nuevo bot buscando a ***@BotFather*** y luego escribiendo `/newbot`.

2. Sigue las indicaciones para crear un nombre y un nombre de usuario para tu bot. El nombre de usuario debe terminar en "bot" y debe ser único.

3. Te darán un token de API HTTP que es tu token de **API BOT** que se usará en Grafana. Tendrá el formato XXXXXXXXX: YYYYYYYYYYYYYYYYYYYYYYYYYYYYY.

4. Luego necesitarás el **CHAT ID**. Para obtenerlo, primero debes crear un grupo y luego agregar el nuevo bot al mismo.

5. Luego, presiona la opción `"Ver información del grupo"` para el grupo, haz clic en el nombre de usuario del bot en la lista de miembros y presiona el botón `"ENVIAR MENSAJE"`.

6. A continuación, envía cualquier mensaje en el grupo donde hayas añadido el bot.

7. Después, en tu navegador, visita la URL https://api.telegram.org/botXXX:YYY/getUpdates (reemplaza XXX:YYY con el token de API BOT que acabas de obtener de Telegram). 
   En la respuesta JSON, deberías ver un nodo con un mensaje que tenga el `type=group`. Este nodo también tendrá un ID. Copia el ID en el campo de CHAT ID en Grafana.

8. Ahora puedes probar el nuevo punto de contacto de Telegram en Grafana utilizando el botón ![](https://i.imgur.com/Z6yqBVc.png).

    Debería haber una alerta de ejemplo de Grafana dentro de la ventana de mensajes del nuevo grupo de Telegram.

![](https://i.imgur.com/igREhq4.png)

!!! Tip "Contact Point persistente"
    Para que este Contact point se quede permanentemente cada vez que se arranca el Grafana hay que añadir un Provisioning file: <https://grafana.com/docs/grafana/latest/alerting/set-up/provision-alerting-resources/file-provisioning/#provision-contact-points>

## Añadir Notification policies:

![](https://i.imgur.com/TVf7m15.png)

1. Lo primero es cambiar el Contact point por defecto al que se desee haciendo clic en ![](https://i.imgur.com/SgEaN63.png):

2. Se selecciona el Contact point por defect, si hay más de uno habŕa que añadir reglas especificas:

    ![](https://i.imgur.com/NmuA5vg.png)

3. Se puede cambiar el agrupamiento que utiliza este contact point, en este caso por defecto si no se añade ningún **Group by** alerta todo:

    ![](https://i.imgur.com/QUeIKjE.png)

4. Por defecto están estos valores en **Timing options**, se puede modificar para que alerte de manera repetida al cambiar el valor de **Group interval** y la opción de **Repeat interval**, una vez creado se guarda ![](https://i.imgur.com/dwTYjCv.png):

    ![](https://i.imgur.com/1hOoi22.png)

!!! Danger "Timing options"
    Un ejemplo sería para que alertara cada 10 minutos una vez que salte la alerta, modificando el valor de `4 Hours` a `10 Minutes`.

*Si se tienen varios Contact point y se quiere añadir diferentes reglas para cada uno, hay que crear reglas especificas en ![](https://i.imgur.com/EkiuEiZ.png):*

- Se selecciona el **Contact point**:

    ![](https://i.imgur.com/ElsASe0.png)

- Se elige la etiqueta por la que se quiera filtrar, en este caso fltro por el nombre de la alerta que es ***HIGH RAM!***:

    ![](https://i.imgur.com/Dx2TQPh.png)

- Por otra parte se puede modificar ***Notification policies*** de esta regla especifica, es decir se puede sobreescribir lo que ya tenía el Contact point por defecto, como por ejemplo el filtrar por grupos en concretos, cambiar solamente en este nueva regla el tiempo de alertas...

    ![](https://i.imgur.com/YBM5qTT.png)

- Al añadir las nuevas reglas, estarán activas hasta que se eliminen o si se reinicia el contenedor donde está el Grafana:

    ![](https://i.imgur.com/jQ6KhS0.png)

!!! Tip "Notification Policies persistente"
    Para que esta configuración se quede permanentemente hay que añadir un ***Provisioning file*** de notification policies en el PATH donde esté montado el Grafana: <https://grafana.com/docs/grafana/latest/alerting/set-up/provision-alerting-resources/file-provisioning/#provision-notification-policies>

# Grafana customizations

!!! Tip "Repositiorio"
    <https://gitlab.com/isard/grafana-customizations>

En Isard tenemos un apartado para añadir **Paneles** y **Alertas** personalizadas de Grafana, para utilizarlo:

- Se añaden los .json de las alertas y paneles ademas de los archivos de configuración .yml en el directorio `/opt/isard/monitor/grafana/custom/`. 

- Se lanza un `bash build.sh`.

- Y se arranca de nuevo el Grafana, `docker compose build isard-grafana && docker compose up -d isard-grafana`.

!!! Example "Ejemplo Alertas"
    Nos clonamos el repositiorio de [customizations](https://gitlab.com/isard/grafana-customizations/gencat-fp) y copiamos la carpeta **alerting** en `/opt/isard/monitor/grafana/custom/`:
    ```
    # ls -lah /opt/isard/monitor/grafana/custom/alerting
    total 60K
    drwxr-xr-x 2 root root 4.0K Jun 14 09:56 .
    drwxr-xr-x 5 root root   52 Feb  7 12:01 ..
    -rw-r--r-- 1 root root 4.0K Mar 13 18:02 alert_engine_dead.json
    -rw-r--r-- 1 root root 3.9K Apr 19 11:49 alert_high_disk_nfs.json
    -rw-r--r-- 1 root root 3.9K Apr 19 11:52 alert_high_disk_opt.json
    -rw-r--r-- 1 root root 2.2K Apr 19 11:49 alert_high_disk_root.json
    -rw-r--r-- 1 root root 3.8K Mar 13 18:02 alert_high_ram.json
    -rw-r--r-- 1 root root 3.7K Mar 13 18:02 alert_high_rethink_connections.json
    -rw-r--r-- 1 root root 2.1K Mar 27 18:45 alert_isard_orchestrator.json
    -rw-r--r-- 1 root root 2.1K May 12 16:44 alert_isard_traceback.json
    -rw-r--r-- 1 root root  462 Mar 14 12:18 alert_message_template_1.yml
    -rw-r--r-- 1 root root  385 Mar 14 12:23 alert_message_template_2.yml
    -rw-r--r-- 1 root root  436 Mar 14 12:24 alert_message_template_3.yml
    -rw-r--r-- 1 root root 2.0K Jun 14 09:56 alert_video_dead.json
    -rw-r--r-- 1 root root  923 May 15 17:01 contact_point_telegram.yml
    -rw-r--r-- 1 root root  119 Mar 14 13:49 notification_policies.yml
    ```

!!! Example "Ejemplo Paneles"
    Nos clonamos el repositiorio de [customizations](https://gitlab.com/isard/grafana-customizations/gencat-fp) y copiamos la carpeta **dashboards** en `/opt/isard/monitor/grafana/custom/`:
    ```
    # ls -lah opt/isard/monitor/grafana/custom/dashboards
    total 104K
    drwxr-xr-x 2 root root 108 Feb 27 11:21 .
    drwxr-xr-x 5 root root  52 Feb  7 12:01 ..
    -rw-r--r-- 1 root root 30K Feb  7 12:01 dashboard_gestor.json
    -rw-r--r-- 1 root root 33K Feb  7 12:01 isard_cost.json
    -rw-r--r-- 1 root root 13K Feb 27 11:21 isard_storage.json
    -rw-r--r-- 1 root root 18K Feb  7 12:01 isard_usage.json
    ```

## Configuración para alertas en Telegram

En el repositorio tenemos un .yml que configura automáticamente el **Contact Point** al arrancar el Grafana para poder enviar alertas al Telgram, en este caso hay dos Contact Point uno para las alertas generales y el otro para solamente logs de `isard-orchestrator`, si no hace falta el de **ORCHESTRATOR**, hay que borrar las líneas para que arranque correctamente el isard-grafana:
```
apiVersion: 1
contactPoints:
  - orgId: 1
    name: telegram-isardvdi
    receivers:
      - uid: telegram-1
        type: telegram
        settings:
          bottoken: "$GRAFANA_TELEGRAM_TOKEN"
          chatid: "$GRAFANA_TELEGRAM_CHAT_ID"
          message: |
            {{ template "default.message" . }}
  - orgId: 1
    name: telegram-orchestrator
    receivers:
      - uid: telegram-2
        type: telegram
        settings:
          bottoken: "$GRAFANA_TELEGRAM_TOKEN_ORCHESTRATOR"
          chatid: "$GRAFANA_TELEGRAM_CHAT_ID_ORCHESTRATOR"
          message: |
            {{ template "default.message" . }}
```
Las variables se configuran en el `isardvdi.cfg`, donde hay que añadir el **TOKEN** del Bot y el **CHAT ID** del grupo de Telegram, el apartado de **ORCHESTRATOR** no hace falta configurarlo si no se desea añadir el Bot de `isard-orchestrator`:

```
# ------ Grafana ------------------------------------------------------
#GRAFANA_HOST=isard-grafana
# This variable is used for provisioning alerts in Grafana
#GRAFANA_TELEGRAM_TOKEN=
#GRAFANA_TELEGRAM_TOKEN_ORCHESTRATOR=
#GRAFANA_TELEGRAM_CHAT_ID=
#GRAFANA_TELEGRAM_CHAT_ID_ORCHESTRATOR=
```

