# Grafana

![](https://i.imgur.com/bKG7AzG.png)

!!! Learn More "Referencia"
    <https://grafana.com/oss/grafana/>

!!! Info "Documentación"
    <https://grafana.com/docs/grafana/latest/>

### Qué es?

**Grafana** es una herramienta muy útil que te ayuda a visualizar y monitorizar datos de forma sencilla, puedes conectarla a diferentes fuentes de datos, como bases de datos o servicios en la nube, para obtener la información que necesitas (logs, métricas...).

### Que funciones tiene?
La parte principal de Grafana son los **"paneles"**. Son como páginas donde puedes crear gráficos, tablas y mapas para mostrar tus datos de manera clara, puedes personalizarlos como quieras incluso hacerlos interactivos.

Grafana también tiene **"plugins"** que te permiten añadir funciones adicionales y personalizar aún más la plataforma. Puedes ampliar las fuentes de datos disponibles, añadir nuevos tipos de gráficos o agregar características especiales según tus necesidades.

Además, Grafana tiene un sistema de **usuarios** y **roles**, lo que significa que puedes controlar quién puede ver y editar los paneles y los datos. Los administradores pueden crear usuarios, asignarles roles y decidir qué partes de Grafana pueden acceder.


![](https://i.imgur.com/5mo8nyY.png)

!!! Learn More "Referencia"
    <https://training.promlabs.com/training/introduction-to-prometheus/installing-and-setting-up-prometheus/using-the-expression-browser>

!!! Info "Documentación"
    <https://prometheus.io/docs/introduction/overview/>

![](https://i.imgur.com/bPQwWfw.png)

### Qué es?

**Prometheus** se encarga de recopilar las métricas de los servicios, **Grafana Agent** se encarga de extraer esas métricas de la base de datos de Prometheus y enviarlas a Grafana.

### Como funciona?

Prometheus tiene una base de datos especial llamada **TSDB (Time Series Database)** para almacenar las métricas que recopila. En lugar de una base de datos tradicional, esta está diseñada para manejar datos que cambian con el tiempo.

En el TSDB de Prometheus, cada métrica se identifica con **etiquetas**, que utiliza una estructura **clave-valor**. Por ejemplo, una etiqueta podría ser el nombre de un servicio o el entorno en el que se encuentra. 

Cuando Prometheus recopila métricas, las guarda en el TSDB usando estas etiquetas. Esto permite realizar consultas rápidas y eficientes para acceder a los datos más adelante. Puedes usar **PromQL (Prometheus Query Language)** para hacer consultas y filtrar los datos basándote en las etiquetas.

!!! Tip "Paneles Grafana IsardVDI"
    ![](https://i.imgur.com/8cXEkTv.png)
