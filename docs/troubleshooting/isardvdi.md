# Resolución de problemas IsardVDI

Cada contenedor tiene sus servicios y su lenguaje. Por poner algunos ejemplos:

- **api**, **engine**: son contenedores con aplicativos en lenguaje *python*
- **authentication**: es un contenedor en lenguaje *GoLang*
- **isard-squid**, **isard-portal**: contenedores con servicios de proxy (squid y haproxy)

Según el problema que tengamos deberemos tener clara la estructura de contenedores, sus funciones y sus relaciones tal como vimos anteriormente.

![Esquema IsardVDI](../sistemas/img/isard-schema.jpg)

Siempre tendremos más información de los errores si ponemos los contenedores en nivel de log **DEBUG**. Por defecto están en **INFO** que no da tantos detalles de los problemas encontrados.


## Problemas con visores:

Deberemos analizar si los contenedores que intervienen están funcionando correctamente:

- isard-hypervisor: es en dónde veremos el escritorio arrancado (virsh list, virsh dumpxml)
- isard-squid: si el problema es de visor **spice** deberemos mirar los logs de conexión dentro del contenedor en `/var/log/squid/access.log`
- isard-vpn: si el problema es de visor **rdp gateway** veremos los logs en este contenedor.

En cualquier caso también es necesario comprobar que los parámetros de vídeo y de carga de ficheros estáticos (javascript) de visores HTML se encuentran bien configurados en el apartado de hypervisores.


## Problemas de start/failed y estados raros

En este caso deberemos analizar los logs de isard-engine. Es recomendable si queremos analizar con detalle cambiar el nivel de logs a *DEBUG* en isardvdi.cfg

También puede ser un problema con el parseado de XML que hace engine. Todos esos logs los veremos también en engine.

En general si hay escritorios que se han quedado en estados raros obtendremos mucha más información de lo que pasó mirando los logs de isard-engine.

Para restablecer los estados raros en caso que sucedieran, podemos forzar el escritorio a *Failed* y después hacer un *start paused*, que hará que engine compruebe de nuevo su estado.

Finalmente siempre tenemos la opción de reiniciar el contenedor de isard-engine. Esta operación se puede realizar con relativa seguridad ya que engine en reiniciar comprueba el estado de todos los hypervisores y escritorios arrancados.


## Problemas de asignación de IP a escritorios

En IsardVDI se usa una red específica `wireguard` para los escritorios que les asigna IP y permite la conexión a través del cliente wireguard.

La red `wireguard` es una vlan interna en el sistema de red virtual OpenVSwitch de Isard. Este sistema conecta en estrella todos los hypervisores con isard-vpn en dónde hay el switch virtual central. Podemos ver los puertos y las vlan asignadas dentro de esos contenedores.

    ovs-vsctl show

En el contenedor de isard-hypervisor podremos ver con `virsh dumpxml` qué vlans tiene conectadas el escritorio. La vlan de wireguard és la 4095, el resto se configuran en el apartado de resources de la web de administración de IsardVDI.

Las conexiones entre hypervisores e isard-vpn son troncales (vlan tagged) dentro de túneles `geneve`.

## Análisis en BBDD

Podemos acceder al entorno web de la BBDD de IsardVDI en el path */debug/db* con el usuario **admin** y la clave que tengamos establecida en *isardvdi.cfg*.

La BBDD es *RethinkDB* y usa su propio lenguaje *reql* [https://rethinkdb.com/docs/introduction-to-reql/](https://rethinkdb.com/docs/introduction-to-reql/)

En el apartado de *data explorer* podremos coger los datos de un id de un escritorio para ver su entrada en la BBDD:

    r.db("isard").table("domains").get("35253f76-11dd-422b-ba4a-fba467df3ef9")

Para analizar correctamente problemáticas en la bbdd hay que tener claros los datos que debemos encontrar. Será necesario por tanto analizar el código como haremos más adelante en los temarios de programación.

# Casos prácticos

Estos casos están basados en  casos reales.

## Red wireguard no da IP

### Punto de partida

Después de un fallo eléctrico se reinicia IsardVDI y el sistema vuelve a estar en funcionamiento.

En el sistema hay varios servidores, escritorios virtuales de Isard que se arrancan automáticamente. Uno de estos servidores es un servidor DHCP y tiene conectada las redes *default* (para salida a Internet) y *dhcp server*, que es una red de tipo OpenVSwitch (ovs) en la VLAN 1800.

Hay que tener claro para este análisis que en IsardVDI:

- La red *default* es una red NAT con salida a Internet, aislada para cada escritorio virtual.
- Las redes *ovs* son VLANS por software (OpenVSwitch) que el administrador crea con un número de VLAN.
- La red *wireguard* es una red por defecto en IsardVDI que permite las conexiones a escritorios virtuales y que internamente es la VLAN 4095 del OpenVSwitch
- Cuando añadimos redes a un escritorio, estas se mapean a su *XML* en el momento de arranque, añadiendo el identificador de VLAN que corresponda a cada una

### Problemática

Los escritorios no reciben dirección IP por la red Wireguard.

### Análisis


1. Miramos qué pasa en los escritorios. Mirando la interfaz wireguard dentro de un escritorio vemos que está dando un rango de direcciones (192.168.x.x) que no corresponde con el rango que da de wireguard (10.x.x.x)

2. Coprobamos las redes que tiene conectadas el escritorio y son las correctas (default y wireguard)

3. Pasamos a analizar si realmente se ha asignado la red correctamente en el escritorio dentro del contenedor *isard-hypervisor*

    docker exec -ti isard-hypervisor /bin/sh
    virsh dumpxml <vm_id>

4. Como todo parece correcto la hipótesis es que hay alguien que ha creado un servidor DHCP en la red wireguard. Buscamos entre los servidores del sistema y encontramos el servidor DHCP de la VLAN 1800. Comprobamos sus redes y solo tiene *default* y *dhcp server*

5. Vamos dentro del hypervisor y comprobamos que el sistema haya asignado correctamente los números de VLAN a cada red del servidor:

    docker exec -ti isard-hypervisor /bin/sh
    virsh dumpxml <vm_id>

Y allí nos damos cuenta que tiene la red default y que en la red *dhcp server* en lugar de asignar la VLAN 1800 ha asignado la vlan wireguard, con lo cual nos está sirviendo DHCP por esa red.

Con un apagado e iniciado del servidor dhcp se solventa el problema que habíamos tenido al iniciar de manera errática isard después de una caida por fallo eléctrico. Sólo ha pasado una vez y en una instalación, ya que no hay explicación concreta de porqué ha pasado.



